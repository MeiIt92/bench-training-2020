Create your own web application testing framework using JavaScript.

Session 1 - 19 May
* Test Automation Pyramid
* Test Automation Framework
* API test automation using Mocha, Chai
* API test automation using Chakram


Session 2 - 26 May
* UI Test Automation Introduction
* Selenium WebDriver API (JavaScript implementation)
* Good Design Patterns for Test Automation
* UI test automation using webdriverIO


Session 3 - 2 June
* UI test automation applying BDD style using webdriverIO
* Data management and parallel execution
* Test report
* Integrate your test to CICD pipeline

Session 4: - 9 June 
Final exam